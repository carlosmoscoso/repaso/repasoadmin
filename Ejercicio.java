public class Ejercicio {

	public static void main(String[] arg) {
		int countZero = 0, notZero = 0;
		int[] array = new int[10];

		System.out.println("Punto 2:");
		for (int i = 0; i < array.length; i++) {
			array[i] = i;
			System.out.println("Posición " + i + " del array tiene el valor: " + array[i]);
		}

		System.out.println("\nPunto 3:");
		for (int i = 0; i < array.length; i++) {
			if (array[i] % 2 == 0) {
				array[i] = 0;
			}
			System.out.println("Posición " + i + " del array tiene el valor: " + array[i]);
		}

		for (int i = 0; i < array.length; i++) {
			if (array[i] == 0) {
				countZero++;
			} else {
				notZero++;
			}
		}

		System.out.println("\nPunto 4:");
		System.out.println("Hay " + countZero + " ceros en el array");
		System.out.println("\nPunto 5:");
		System.out.println("Hay " + notZero + " elementos distintos de cero en el array");
	}

}